#include "elipsa.h"



elipsa::elipsa()
{
}


elipsa::elipsa(int poludniky, int rovnobezky, double hlav, double vedl) {
	na=poludniky;
	nb=rovnobezky;
	a = hlav;
	b = vedl;
	int N = nb*na - nb + 2;

	x = new double[N];
	y = new double[N];
	z = new double[N];

	x[0] = a;
	y[0] = 0;
	z[0] = 0;
	rataj_body();
	x[N - 1] = -a;
	y[N - 1] = 0;
	z[N - 1] = 0;
}

void elipsa::rataj_body() {
	int poc = 1;

	for (int i=1;i<na;i++)
		for (int j = 0; j < nb; j++) {
			x[poc] = a*cos(j * 2 * M_PI / nb)*cos(i*M_PI / na);
			y[poc] = a*cos(j * 2 * M_PI / nb)*sin(i*M_PI / na);
			z[poc] = b*sin(j * 2 * M_PI / nb);
			poc++;
		}

}

Bod elipsa::operator[](int i) {
	return Bod(x[i], y[i],z[i]);
}

elipsa::~elipsa()
{
	delete[] x;
	delete[] y;
	delete[] z;
}
