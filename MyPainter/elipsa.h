#pragma once
#include <cmath>
#include "Bod.h"

using namespace std;

class elipsa
{
public:
	elipsa();
	elipsa(int, int,double,double);
	Bod operator[](int);
	~elipsa();
private:
	void rataj_body();

	double* x;
	double* y;
	double* z;
	int na,nb;
	double a, b;
};

